{ config, ... }:
{
  programs.imv = {
    enable = true;
    settings = {
      options = {
        background = "${config.colorScheme.palette.base00}";
        scaling_mode = "shrink";
        overlay_font = "FantasqueSansMNerdFont:12";
        overlay_text_color = "${config.colorScheme.palette.base05}";
        overlay_background_color = "${config.colorScheme.palette.base01}";
      };
    };
  };
}
