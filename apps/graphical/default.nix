{
  imports = [
    ./alacritty
    ./firefox
    ./foot
    ./imv
    ./kde-connect
    ./mpv
    ./sioyek
    ./kitty
    # ./spicetify
  ];
}
