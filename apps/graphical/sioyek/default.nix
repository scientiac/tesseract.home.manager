{ config, inputs, ... }:
let
  # Convert From HEH to RGB01 String
  rgbToRGB01String = rgb: toString (rgb / 255.0);
  hexToRGB01String =
    hex:
    let
      rgb = inputs.nix-colors.lib-core.conversions.hexToRGB hex;
      rgb01 = builtins.map rgbToRGB01String rgb;
    in
    builtins.concatStringsSep " " rgb01;

  # Convert the background color
  backgorundColor = hexToRGB01String config.colorScheme.palette.base00;
  statusBarColor = hexToRGB01String config.colorScheme.palette.base01;
  statusBarTextColor = hexToRGB01String config.colorScheme.palette.base06;
in
{
  programs.sioyek = {
    enable = true;
    config = {
      "status_bar_color" = statusBarColor;
      "status_bar_text_color" = statusBarTextColor;
      "status_bar_font_size" = "13";
      "ui_font" = "FantasqueSansMNerdFont";
      "background_color" = backgorundColor;
    };
  };
}
