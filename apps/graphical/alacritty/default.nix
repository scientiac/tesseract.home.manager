{ config, ... }: {
    programs.alacritty = {
	enable = true;
	settings = {
	    window = {
		opacity = 0.9;
		padding.x = 10;
		padding.y = 5;
		decorations = "none";
		startup_mode = "maximized";
	    };
	    font = {
		size = 12.0;

		normal.family = "FantasqueSansMNerdFont";
		bold.family = "FantasqueSansMNerdFont";
		italic.family = "FantasqueSansMNerdFont";
	    };
	    cursor.style = "Beam";
	    colors = {
		primary = {
		    background= "0x${config.colorScheme.palette.base00}";
		    foreground= "0x${config.colorScheme.palette.base05}";
		};

		normal = {
		    black=   "0x${config.colorScheme.palette.base00}";
		    red=     "0x${config.colorScheme.palette.base08}";
		    green=   "0x${config.colorScheme.palette.base0B}";
		    yellow=  "0x${config.colorscheme.palette.base0A}";
		    blue=    "0x${config.colorscheme.palette.base0D}";
		    magenta= "0x${config.colorscheme.palette.base0E}";
		    cyan=    "0x${config.colorscheme.palette.base0C}";
		    white=   "0x${config.colorScheme.palette.base05}";
		};

		bright = {
		    black=   "0x${config.colorScheme.palette.base03}";
		    red=     "0x${config.colorScheme.palette.base08}";
		    green=   "0x${config.colorScheme.palette.base0B}";
		    yellow=  "0x${config.colorscheme.palette.base0A}";
		    blue=    "0x${config.colorscheme.palette.base0D}";
		    magenta= "0x${config.colorscheme.palette.base0E}";
		    cyan=    "0x${config.colorscheme.palette.base0C}";
		    white=   "0x${config.colorscheme.palette.base07}";
		};
	    };
	};
    };
}
