{
  imports = [
    ./terminal
    ./graphical
  ];

  xdg.mimeApps = {
    enable = true;

    defaultApplications = {
      "application/pdf" = "sioyek.desktop";
      "text/markdown" = "helix.desktop";
      "text/plain" = "helix.desktop";
      "text/html" = "firefox.desktop";
      "image/png" = "imv.desktop";
      "image/jpeg" = "imv.desktop";
      "image/gif" = "imv.desktop";
      "image/svg" = "org.inkscape.Inkscape.desktop";
      "image/svg+xml" = "org.inkscape.Inkscape.desktop";
      "x-scheme-handler/http" = "firefox.desktop";
      "x-scheme-handler/https" = "firefox.desktop";
      # "x-scheme-handler/mailto" = "thunderbird.desktop";
    };
  };
}
