{
 programs.newsboat = {
		enable = true;
		autoReload = true;
		extraConfig = "
		    # unbind keys
		    unbind-key ENTER
		    unbind-key j
		    unbind-key k
		    unbind-key J
		    unbind-key K

		    # bind keys - vim style
		    bind-key j down
		    bind-key k up
		    bind-key l open
		    bind-key h quit
		";

		urls = [
		    {
			title = "Scientiac";
			tags = [
			    "People"
			];
			url = "https://scientiac.space/atom.xml";
		    }
		    {

			title = "This Week in GNOME";
			tags = [
			    "FOSS"
			];
			url = "https://thisweek.gnome.org/index.xml";
		    }
		    {

			title = "Redox OS";
			tags = [
			    "FOSS"
			];
			url = "https://www.redox-os.org/index.xml";
		    }
		    {

			title = "System76";
			tags = [
			    "FOSS"
			];
			url = "https://blog.system76.com/rss.xml";
		    }
		    {

			title = "Benjamin Hollon";
			tags = [
			    "People"
			];
			url = "https://benjaminhollon.com/feed/?from=musings%2Ctty1%2Cpoetry";
		    }
		    {

			title = "Mo8it";
			tags = [
			    "People"
			];
			url = "https://mo8it.com/atom.xml";
		    }
		    {

			title = "Dom Corriveau";
			tags = [
			    "People"
			];
			url = "https://blog.ctms.me/index.xml";
		    }
		    {

			title = "Bartosz Ciechanowski";
			tags = [
			    "People"
			];
			url = "https://ciechanow.ski/atom.xml";
		    }
		    {

			title = "XKCD";
			tags = [
			    "Fun"
			];
			url = "https://xkcd.com/atom.xml";
		    }
		];
	};
}
