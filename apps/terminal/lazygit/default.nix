{
  programs.lazygit = {
    enable = true;
    settings = {
      gui = {
        language = "auto";
        mouseEvents = true;
      };
    };
  };
}
