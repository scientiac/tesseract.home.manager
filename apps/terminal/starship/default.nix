{
    programs.starship = {
	enable = true;
	settings = {
	    custom.direnv = {
		format = "[\\[direnv\\]]($style) ";
		style = "fg:yellow dimmed";
		when = "env | grep -E '^DIRENV_FILE='";
	    };
	};
    };
}
