{ config, ...}: {
    programs.zellij = {
	enable = true;
	# enableBashIntegration = true;
	settings = {
	    simplified_ui = true;
	    pane_frames = false;
	    default_layout = "compact";
	    theme = "custom";
	    themes.custom = {
		    fg = "#${config.colorScheme.palette.base05}";
		    bg = "#${config.colorScheme.palette.base00}";
		    black = "#${config.colorScheme.palette.base00}";
		    red = "#${config.colorScheme.palette.base08}";
		    green = "#${config.colorScheme.palette.base0B}";
		    yellow = "#${config.colorscheme.palette.base0A}";
		    blue = "#${config.colorscheme.palette.base0D}";
		    magenta = "#${config.colorscheme.palette.base0E}";
		    cyan = "#${config.colorscheme.palette.base0C}";
		    white = "#${config.colorScheme.palette.base05}";
		    orange = "#${config.colorScheme.palette.base09}";
	    };
	    ui = {
		pane_frames = {
		    hide_session_name = true;
		    rounded_corners = true;
		};
	    };
	};
    };
}
