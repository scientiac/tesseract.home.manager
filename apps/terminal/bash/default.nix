{ pkgs, ... }:
{
  imports = [ ./scripts ];

  home.packages = with pkgs; [
    trash-cli
    eza
  ];

  programs.bash = {
    enable = true;
    shellAliases = {
      ls = "eza --group-directories-first --icons --color=always --tree -L 1";
      rm = "trash";
      config = "git --git-dir=/home/scientiac/.config/tesseract/config/ --work-tree=/home/scientiac";
      sioyek = "sioyek $@ > /dev/null 2>&1";
    };

    sessionVariables = {
      EDITOR = "nvim";
    };

    initExtra = "source ${./scripts/extra.sh}";
  };
}
