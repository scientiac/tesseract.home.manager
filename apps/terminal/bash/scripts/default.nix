{ pkgs, ... }:

let
  zellij-attach = pkgs.writeShellScriptBin "zellij-auto-attach" (builtins.readFile ./zellij.sh);
  plate = pkgs.writeShellScriptBin "plate" (builtins.readFile ./plate.sh);
in
{
  home.packages = with pkgs; [
    skim
    fzf
    gnused
    gawk

    # bash scripts
    zellij-attach
    plate
  ];
}
