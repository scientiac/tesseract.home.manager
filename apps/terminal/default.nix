{
  imports = [
    ./bash
    ./direnv
    ./git
    ./helix
    ./neovim
    ./newsboat
    ./starship
    ./zellij
    ./zoxide
    ./lazygit
    ./ncmpcpp
  ];
}
