{ pkgs, ... }:
{
  programs.neovim = {
    enable = true;
    extraLuaPackages = ps: [ ps.magick ];

    extraPackages = with pkgs; [
        # dependencies.nvim
        fd
        fzf
        ripgrep
        unzip
        gcc

        # image.nvim
        imagemagick

        # lsp.nvim
        nil
        lua-language-server
        bash-language-server
    ];

    extraLuaConfig = ''
        vim.loader.enable()
        require("core.opts").initial()
        local lazy_path = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
        require("core.utils").bootstrap(lazy_path)
        require "plugins"
    '';
  };
}
