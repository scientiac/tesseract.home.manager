{ inputs, pkgs, ... }:
{
  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = _: true;
    };
  };

  imports = [
    inputs.nix-colors.homeManagerModules.default
    ./apps
    ./desktop
  ];

  # colorScheme = inputs.nix-colors.colorSchemes.gruvbox-light-hard;
  colorScheme = inputs.nix-colors.colorSchemes.monokai;

  gtk = {
    enable = true;
    theme = {
      package = pkgs.adw-gtk3;
      name = "adw-gtk3-dark";
    };
    iconTheme = {
      name = "Adwaita";
      package = pkgs.adwaita-icon-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "qt6ct";
  };

  home.pointerCursor = {
    gtk.enable = true;
    package = pkgs.bibata-cursors;
    name = "Bibata-Original-Classic";
    size = 16;
  };

  home.sessionVariables = {
    NIXOS_OZONE_WL = "1";

    QT_STYLE_OVERRIDE = "adwaita-dark";
    # QT_STYLE_OVERRIDE = "adwaita";

    EDITOR = "nvim";

    GTK_IM_MODULE = "fcitx";
    QT_IM_MODULE = "fcitx";
    SDL_IM_MODULE = "fcitx";
    XMODIFIERS = "@im=fcitx";
  };

  dconf.settings = { };

  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "scientiac";
  home.homeDirectory = "/home/scientiac";

  fonts.fontconfig.enable = true;

  # Install packages
  home.packages = with pkgs; [
    #qt 
    kdePackages.qt6ct
    libsForQt5.qt5ct
    kdePackages.breeze-icons
    adwaita-qt
    adwaita-qt6

    #GTK
    nautilus
    sushi

    # trash
    trash-cli

    #paru
    publii

    # Slides
    presenterm

    # nerd fonts
    (nerdfonts.override {
      fonts = [
        "Meslo"
        "FantasqueSansMono"
        "Mononoki"
        "NerdFontsSymbolsOnly"
      ];
    })

    # Matlab
    octaveFull

    # communication
    konversation
    telegram-desktop

    # signal and cli
    signal-desktop
    scli

    #download
    aria
    magic-wormhole-rs

    # tui media
    tut
    amfora

    # typst
    typst

    # screen capture and video
    ffmpeg
    wf-recorder

    # graphics
    krita
    gimp
    inkscape

    # communication
    webcord-vencord
    beeper
    zoom-us

    # neovim
    fd
    fzf
    ripgrep
    unzip

    # for remote screen control
    scrcpy

    # too long didn't read
    tldr

    # Torrent
    webtorrent_desktop

    #sixel
    lsix
    chafa

    # for scientiac.space
    zola

    #for hugo robotics.ioepc.edu.np
    hugo
    go

    # browser
    brave
    chromium

    #latex
    texliveSmall
    pandoc
    pdftk
  ];

  home.file = { };

  systemd.user.startServices = true;

  # Please read the comment before changing.
  home.stateVersion = "23.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
