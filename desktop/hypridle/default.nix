{
  config,
  pkgs,
  lib,
  ...
}:
{
  # home.packages = with pkgs; [ hypridle ];

  services.hypridle = {
    enable = true;
    settings = {
      general = {
        lock_cmd = "pidof hyprlock || ${lib.getExe pkgs.hyprlock} --immediate";
        unlock_cmd = "pkill -USR1 hyprlock";

        before_sleep_cmd = "${lib.getExe' pkgs.systemd "loginctl"} lock-session";
        after_sleep_cmd = "hyprctl dispatch dpms on";
      };

      listener = [
        {
          timeout = 600;
          on-timeout = "pidof hyprlock || ${lib.getExe pkgs.hyprlock}";
        }
        {
          timeout = 800;
          on-timeout = "hyprctl dispatch dpms off";
          on-resume = "hyprctl dispatch dpms on";
        }
        {
          timeout = 1000;
          on-timeout = "${lib.getExe' pkgs.systemd "systemctl"} suspend";
        }
      ];
    };
  };

  #  xdg.configFile."hypr/hypridle.conf".text = ''
  #    general {
  #        ignore_dbus_inhibit = false
  # lock_cmd = hyprlock
  # before_sleep_cmd = hyprlock
  #    }

  #    # Screenlock
  #    listener {
  #        timeout = 600
  #        on-timeout = hyprlock
  #        on-resume = notify-send "Welcome back!"
  #    }

  #    # Suspend (not working on my laptop)
  #    listener {
  #        timeout = 660
  #        on-timeout = systemctl suspend
  #        on-resume = notify-send "Welcome back to your desktop!"
  #    }
  #  '';
}
