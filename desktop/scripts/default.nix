{
  imports = [
    ./screenshot.nix
    ./brightness.nix
    ./sound.nix
    ./caffeine.nix
    ./nightmode.nix
    ./tess.nix
    ./quark.nix
  ];
}
