{ pkgs, ... }:

let
  # Script to send files or URLs to the device
  quark-send = pkgs.writeShellScriptBin "quark-send" ''
    DEVICE_NAME="quark"
    item_to_send="$1"

    if [ -f "$item_to_send" ]; then
      ${pkgs.kdeconnect}/bin/kdeconnect-cli --share "$item_to_send" --name "$DEVICE_NAME"
      echo "File '$item_to_send' sent to device '$DEVICE_NAME'."
    elif [[ "$item_to_send" =~ ^https?:// ]]; then
      ${pkgs.kdeconnect}/bin/kdeconnect-cli --share "$item_to_send" --name "$DEVICE_NAME"
      echo "URL '$item_to_send' sent to device '$DEVICE_NAME'."
    else
      echo "File or URL '$item_to_send' not found."
    fi
  '';

  # Script to ring the device
  quark-ring = pkgs.writeShellScriptBin "quark-ring" ''
    DEVICE_NAME="quark"
    ${pkgs.kdeconnect}/bin/kdeconnect-cli --ring --name "$DEVICE_NAME"
    echo "Device '$DEVICE_NAME' is ringing."
  '';

  # Script to send clipboard text to the device
  quark-clipboard = pkgs.writeShellScriptBin "quark-clipboard" ''
    DEVICE_NAME="quark"
    clipboard_text=$(${pkgs.wl-clipboard}/bin/wl-paste)

    if [ -n "$clipboard_text" ]; then
      ${pkgs.kdeconnect}/bin/kdeconnect-cli --share-text "$clipboard_text" --name "$DEVICE_NAME" > /dev/null 2>&1
      echo "Clipboard text sent to device '$DEVICE_NAME'."
    else
      echo "Clipboard is empty."
    fi
  '';

  # Main script to handle different actions
  quark = pkgs.writeShellScriptBin "quark" ''
    DEVICE_NAME="quark"

    case "$1" in
      send)
        if [ -z "$2" ]; then
          echo "Usage: $0 send <file_or_url_to_send>"
          exit 1
        fi
        quark-send "$2"
        ;;
      
      ring)
        quark-ring
        ;;
      
      clipboard)
        quark-clipboard
        ;;
      
      *)
        echo "Usage: $0 {send|ring|clipboard}"
        exit 1
        ;;
    esac
  '';

in
{
  home.packages = [
    quark-send
    quark-ring
    quark-clipboard
    quark
  ];
}
