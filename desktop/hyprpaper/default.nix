{ config, ... }:
{
  services.hyprpaper = {
    enable = true;
    settings = {
      preload = [ "${./${config.colorScheme.slug}.png}" ];
      wallpaper = [ ",${./${config.colorScheme.slug}.png}" ];
      splash = true;
      ipc = false;
    };
  };
}
