{
  services.wlsunset = {
    enable = true;
    systemdTarget = "graphical-session.target";

    latitude = "26.7";
    longitude = "87.2";
  };
}
