{
  imports = [
    ./fcitx5
    ./hypridle
    ./hyprland
    ./hyprlock
    ./hyprpaper
    ./menu
    ./notification
    ./scripts
    ./swappy
    ./waybar
    ./wlsunset
  ];
}
