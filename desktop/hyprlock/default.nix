{ config, ... }:
{
  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        disable_loading_bar = true;
        hide_cursor = true;
        no_fade_in = false;
      };
      background = [
        {
          path = "$HOME/.config/tesseract/home-manager/desktop/hyprpaper/${config.colorScheme.slug}.png";
          blur_passes = 3;
          blur_size = 8;
        }
      ];
      image = [
        {
          path = "$HOME/scientiac.png";
          size = 150;
          border_size = 4;
          border_color = "rgb(${config.colorScheme.palette.base07})";
          rounding = -1;
          position = "0, 200";
          halign = "center";
          valign = "center";
        }
      ];
      input-field = [
        {
          size = "200, 40";
          position = "0, -250";
          monitor = "";
          rounding = "-1";
          dots_center = true;
          fade_on_empty = true;
          font_color = "rgb(${config.colorScheme.palette.base07})";
          inner_color = "rgb(${config.colorScheme.palette.base00})";
          outer_color = "rgb(${config.colorScheme.palette.base03})";
          outline_thickness = 2;
          placeholder_text = "";
          shadow_passes = 2;
        }
      ];
    };
  };
}
