{ pkgs, ... }:

let

  powermenu = pkgs.writeShellScriptBin "powermenu" ''

        entries="Lock\nLogout\nReboot\nSuspend\nShutdown"

        selected=$(pgrep wofi && pkill wofi || echo -e $entries |  wofi --xoffset=1186 --yoffset=6  --width 40 --height 173 --dmenu --prompt=Power -Ddynamic_lines=false -Dcontent_halign=center )

        case $selected in
          Logout)
          	hyprctl dispatch exit;;
          suspend)
    	      exec systemctl suspend;;
          Reboot)
    	      exec systemctl reboot;;
          Shutdown)
    	      exec systemctl poweroff -i;;
          Lock)
    	      exec hyprlock;;
        esac

  '';

in
{
  home.packages = [ powermenu ];
}
