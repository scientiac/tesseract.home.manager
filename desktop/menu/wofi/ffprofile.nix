{ pkgs, ... }:

let

  ffprofile = pkgs.writeShellScriptBin "ffprofile" ''

    entries="󱄅  scientiac\n   spandan"

    selected=$(pgrep wofi && pkill wofi || echo -e $entries | wofi -n --height 95 --width 100 -I --dmenu --prompt=firefox --cache-file /dev/null | awk '{print tolower($2)}')

    case $selected in
      scientiac)
	 exec firefox -no-remote -p scientiac;;
      spandan)
	 exec firefox -no-remote -p spandan;;
    esac

  '';

in { home.packages = [ ffprofile ]; }
