{
  imports = [
    ./appmenu.nix
    ./clipboard.nix
    ./powermenu.nix
    ./imgclip.nix
    ./ffprofile.nix
  ];
}
