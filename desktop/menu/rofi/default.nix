{
  config,
  lib,
  pkgs,
  ...
}:
{
  programs.rofi = {
    enable = true;
    package = pkgs.rofi-wayland;

    font = "FantasqueSansMNerdFont 12";
    terminal = "footclient";

    plugins = with pkgs; [
      rofi-calc
      rofi-power-menu
    ];

    extraConfig = {
      show-icons = true;
      drun-display-format = "{icon} {name}";
      disable-history = false;
      hide-scrollbar = false;
      sidebar-mode = true;

      run-shell-command = "{terminal} --hold {cmd}";

      display-drun = "   Apps ";
      display-run = "   Run ";
      display-power-menu = "   Power ";

      modi = lib.strings.concatStringsSep "," [
        "run"
        "drun"
        "power-menu:${lib.getExe pkgs.rofi-power-menu}"
      ];
    };

    theme =
      let
        inherit (config.lib.formats.rasi) mkLiteral;
      in
      {
        "*" = {
          bg-col = mkLiteral "#${config.colorScheme.palette.base00}";
          bg-col-light = mkLiteral "#${config.colorScheme.palette.base01}";
          border-col = mkLiteral "#${config.colorScheme.palette.base0B}";
          selected-col = mkLiteral "#${config.colorScheme.palette.base0B}";
          orange = mkLiteral "#${config.colorScheme.palette.base09}";
          fg-col = mkLiteral "#${config.colorScheme.palette.base0B}";
          fg-col2 = mkLiteral "#${config.colorScheme.palette.base00}";
          grey = mkLiteral "#${config.colorScheme.palette.base02}";

          width = 600;
        };

        "element-text, element-icon , mode-switcher" = {
          background-color = mkLiteral "inherit";
          text-color = mkLiteral "inherit";
        };

        window = {
          height = mkLiteral "360px";
          border = mkLiteral "2px";
          border-color = mkLiteral "@border-col";
          border-radius = mkLiteral "8px";
          background-color = mkLiteral "@bg-col";
        };

        mainbox = {
          background-color = mkLiteral "@bg-col";
        };

        inputbar = {
          children = [
            (mkLiteral "prompt")
            (mkLiteral "entry")
          ];
          background-color = mkLiteral "@bg-col";
          border-radius = mkLiteral "5px";
          padding = mkLiteral "2px";
        };

        prompt = {
          background-color = mkLiteral "@orange";
          padding = mkLiteral "6px";
          text-color = mkLiteral "@bg-col";
          border-radius = mkLiteral "3px";
          margin = mkLiteral "20px 0px 0px 20px";
        };

        textbox-prompt-colon = {
          expand = false;
          str = ":";
        };

        entry = {
          padding = mkLiteral "6px";
          margin = mkLiteral "20px 0px 0px 10px";
          text-color = mkLiteral "@fg-col";
          background-color = mkLiteral "@bg-col";
        };

        listview = {
          border = mkLiteral "0px 0px 0px";
          padding = mkLiteral "6px 0px 0px";
          margin = mkLiteral "10px 0px 0px 20px";
          columns = 2;
          lines = 5;
          background-color = mkLiteral "@bg-col";
        };

        element = {
          padding = mkLiteral "5px";
          background-color = mkLiteral "@bg-col";
          text-color = mkLiteral "@fg-col";
        };

        element-icon = {
          size = mkLiteral "25px";
        };

        "element selected" = {
          background-color = mkLiteral "@selected-col";
          text-color = mkLiteral "@fg-col2";
        };

        mode-switcher = {
          spacing = 0;
        };

        button = {
          padding = mkLiteral "10px";
          background-color = mkLiteral "@bg-col-light";
          text-color = mkLiteral "@grey";
          vertical-align = mkLiteral "0.5";
          horizontal-align = mkLiteral "0.5";
          border-radius = mkLiteral "5px";
        };

        "button selected" = {
          background-color = mkLiteral "@bg-col";
          text-color = mkLiteral "@orange";
        };

        message = {
          background-color = mkLiteral "@bg-col-light";
          margin = mkLiteral "2px";
          padding = mkLiteral "2px";
          border-radius = mkLiteral "5px";
        };

        textbox = {
          padding = mkLiteral "6px";
          margin = mkLiteral "20px 0px 0px 20px";
          text-color = mkLiteral "@orange";
          background-color = mkLiteral "@bg-col-light";
        };
      };
  };

  xdg.configFile."rofimoji.rc".text = "action = copy";
}
